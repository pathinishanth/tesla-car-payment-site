#!/bin/bash

echo "Updating Repo"
sudo apt-get update -y

echo "Installing Pre-Requisites"
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release -y


echo "Docker’s official GPG key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg


echo "Add Stable Repository"
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


echo "Update Repo with Docker"
sudo apt-get update -y

echo "Install Docker Engine"
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

echo "Adding the Current user to Docker Group"
sudo usermod -aG docker $USER

echo "Please logout of the Server and Login Again"
echo "After login run the below command"
echo "docker version"